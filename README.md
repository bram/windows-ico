# windows-ico
A shell script that creates a windows icon with a label out of a square image.

Usage:

```
windows_ico.sh <source_image> <label_text>
```

The resulting `.ico` file contains all required layers and is saved in the same directory as the source image.


## Example

```
./windows_ico.sh ./example/firefox.png ICO
```

Creates a Firefox icon with the label `ICO`.
GitLab can't show the resulting `.ico` file here, but these are the images created by `convert firefox.ico result.png`. Showing the individual layers of the ico file:

- `result-0.png` ![result-0](example/result-0.png)
- `result-1.png` ![result-1](example/result-1.png)
- `result-2.png` ![result-2](example/result-2.png)
- `result-3.png` ![result-3](example/result-3.png)
- `result-4.png` ![result-4](example/result-4.png)
- `result-5.png` ![result-5](example/result-5.png)
- `result-6.png` ![result-6](example/result-6.png)