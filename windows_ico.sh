#!/bin/bash

# Function to display script usage/help
show_usage() {
  echo "Usage: $0 <source_image> <label_text>"
  echo "  <source_image>: Path to the source image (should be square)."
  echo "  <label_text>: Text to add to the lower right corner of the image (up to 4 characters)."
}

# Function to add a character to the lower right corner of an image
add_character_to_image() {
  input_image=$1
  character=$2
  output_image=$3

  convert "$input_image" -resize 256x256 -fill '#908cc3' -font 'Helvetica-Bold' -undercolor '#00000090' -pointsize 70 -gravity southeast -annotate +2+25 "$character" "$output_image"
}

# Function to generate a Windows 10 ICO file with multiple sizes from an input image
generate_windows_ico() {
  input_image=$1
  output_ico=$2

  convert -define icon:auto-resize="256,128,96,64,48,32,16" "$input_image" "$output_ico"
}

# Check if ImageMagick is installed
if ! command -v convert &>/dev/null; then
  echo "ImageMagick not found. Please install it first."
  exit 1
fi

# Check the number of arguments
if [ $# -ne 2 ]; then
  show_usage
  exit 1
fi

# Assign arguments to variables
source_image=$1
label_text=$2

# Check if the source image exists
if [ ! -f "$source_image" ]; then
  echo "Error: Source image '$source_image' not found."
  exit 1
fi

# Check if the label text exceeds 6 characters
if [ ${#label_text} -gt 6 ]; then
  echo "Error: Label text should have up to 6 characters."
  exit 1
fi

# Check if the image is square
image_size=$(identify -format "%wx%h" "$source_image")
width=${image_size%x*}
height=${image_size#*x}

if [ "$width" -ne "$height" ]; then
  echo "Error: Source image is not square. Width: $width, Height: $height"
  exit 1
fi

# Create temporary files
temp_image=$(mktemp --suffix=.png)
temp_icon=$(mktemp --suffix=.ico)

# Generate the ICO file
add_character_to_image "$source_image" "$label_text" "$temp_image"
generate_windows_ico "$temp_image" "$temp_icon"

# Move the generated ICO file to the source image directory
output_filename=$(basename "$source_image" | sed 's/\.[^.]*$//').ico
mv "$temp_icon" "$(dirname "$source_image")/$output_filename"

# Clean up temporary files
rm "$temp_image"